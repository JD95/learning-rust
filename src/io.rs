// Gets input from the console that must be valid
// according to the `valid` function.
//
// It will loop and print error messages until a valid
// input is given.
pub fn get_valid_input<F, Output>(valid: F) -> Output
where
    F: Fn(String) -> Result<Output, String>,
{
    // We need to loop until the valid input is given
    loop {
        // The read_line function doesn't return
        // a value, it mutates an existing string
        let mut buf: String = String::new();

        // read_line returns a Result type, so there's
        // two cases we need to handle
        match std::io::stdin().read_line(&mut buf) {
            // If it's Ok, then reading the input
            // was successful and `buf` will be filled
            // with what the user typed in
            //
            // Since we have that input, we can
            // validate what's in `buf`
            Ok(_) => match valid(buf) {
                // In this case, the input was
                // correct and has been parsed into
                // the type we want
                Ok(value) => {
                    // Using a return statement
                    // here let's us break out of the loop
                    return value;
                }
                // The input from the user was not
                // valid
                Err(err) => {
                    // We just print the error message
                    // Note that print! returns `()`
                    // so if there's an error here, then
                    // the entire match expression evaluates
                    // to `()` and the loop will continue
                    print!("{}", err)
                }
            },
            // If reading from the console failed
            // then we'll just return nothing `()`
            // and finish this iteration of the loop
            _ => (),
        }
    }
}

// Gets a single i32 from the console
pub fn get_i32() -> i32 {
    get_valid_input(|s| {
        s.trim()
            .parse()
            .map_err(|_| "input must be an i32!".to_string())
    })
}
