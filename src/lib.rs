mod io;

fn words(input: String) -> Vec<String> {
    let pats = vec![" ", ",", "."];
    let mut result = vec![input];
    for pat in pats {
        result = result
            .iter()
            .flat_map(|x: &String| x.split(pat).map(|x| x.to_string()))
            .filter(|x| !x.is_empty())
            .collect();
    }
    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_words() {
        let expected = vec!["hello", "world"];
        assert_eq!(words("hello world".to_string()), expected);
        assert_eq!(words("hello, world".to_string()), expected);
        assert_eq!(words("hello,world.".to_string()), expected);
    }
}
